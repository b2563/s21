console.log("Happy Thursday!");

// An array in programming is isimply a list of data

let studentNumberA = '2023-1923';
let studentNumberB = '2023-1924';
let studentNumberC = '2023-1925';
let studentNumberD = '2023-1926';
let studentNumberE = '2023-1927';

let studentNumbers = ['2023-1923', '2023-1924', '2023-1925', '2023-1926', '2023-1927'];

let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

let mixedArr = [12, 'Asus', null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

let myTasks = [
    'drink HTML',
    'eat JavaScript',
    'inhale CSS',
    'bake SASS'
];

console.log(myTasks);

let city1 = 'Tokyo';
let city2 = 'Manila';
let city3 = 'Jakarta';

let cities = [city1, city2, city3];

console.log(cities);

console.log(myTasks.length);
console.log(cities.length);

let blankArr = myTasks.length -1;
console.log(blankArr,length);
console.log(myTasks);

cities.length--;
console.log(cities.length);
console.log(cities);

let theBeatles = ['John', 'Paul', 'Ringo', 'George'];
theBeatles.length++;
console.log(theBeatles);

/*
    - Accessing array elements is one of the more common tasks that we do with an array
    - This can be done through the use of array indexes
    - Each element in an array is associated with it's own index/number
    - In JavaScript, the first element is associated with the number 0 and increasing this number by 1 for every element
    - The reason an array starts with 0 is due to how the language is designed
- Array indexes actually refer to an address/location in the device's memory and how the information is stored
    - Example array location in memory
        Array address: 0x7ffe9472bad0
        Array[0] = 0x7ffe9472bad0
        Array[1] = 0x7ffe9472bad4
        Array[2] = 0x7ffe9472bad8
    - In the example above, the first element and the array itself points to the same memory location and therefore is at 0 elements away from the location of the array itself
- Syntax
        arrayName[index];
*/

console.log(grades[2]);
console.log(computerBrands[3]);

let lakersLegends = ['Kobe', 'Shaq', 'Lebron', 'Magic', 'Kareem'];

console.log(lakersLegends[1]);

console.log(lakersLegends[3]);

let currentLakers = lakersLegends[2];
console.log(currentLakers);

console.log("Array bfore reassignment");
console.log(lakersLegends);
lakersLegends[2] = 'Pau Gasol';
console.log('Array after reassignment');
console.log(lakersLegends);

let lastIndexElement = lakersLegends.length - 1;
console.log(lakersLegends[lastIndexElement]);

console.log(lakersLegends[lakersLegends.length - 1]);

let newArr = [];
console.log(newArr[0]);

newArr[1] = "Cloud Strife";
console.log(newArr);

newArr[0] = 'Tifa Lockhart';
console.log(newArr);

newArr[newArr.length] = "Barret Wallace";
console.log(newArr);

for (let index = 0; index < newArr.length; index++) {
    console.log(newArr[index]);
};

let numArr = [5, 12, 30, 46, 40];

for(let index = 0; index < numArr.length; index++){
    if(numArr[index] % 5 === 0) {
        
        console.log(numArr[index] + ' is divisible by 5')

    } else {

        console.log(numArr[index] + ' is not divisible by 5')
    }
}

/*
    - Accessing array elements is one of the more common tasks that we do with an array
    - This can be done through the use of array indexes
    - Each element in an array is associated with it's own index/number
    - In JavaScript, the first element is associated with the number 0 and increasing this number by 1 for every element
    - The reason an array starts with 0 is due to how the language is designed
- Array indexes actually refer to an address/location in the device's memory and how the information is stored
    - Example array location in memory
        Array address: 0x7ffe9472bad0
        Array[0] = 0x7ffe9472bad0
        Array[1] = 0x7ffe9472bad4
        Array[2] = 0x7ffe9472bad8
    - In the example above, the first element and the array itself points to the same memory location and therefore is at 0 elements away from the location of the array itself
- Syntax
        arrayName[index];
*/

//You can use a for loop to iterate over all items in an array.
//Set the counter as the index and set a condition that as long as the current index iterated is less than the length of the array, we will run the condition. It is set this way because the index of an array starts at 0.

/*
    - Multidimensional arrays are useful for storing complex data structures
    - A practical application of this is to help visualize/create real world objects
    - Though useful in a number of cases, creating complex array structures is not always recommended.
*/
let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard);
// Accessing elements of a multidimensional arrays
console.log(chessBoard[1][4]);

console.log("Pawn moves to: " + chessBoard[1][5]);